import React from 'react'
import { NavLink } from 'react-router-dom'

const ButtonsButtom = (props) => {
    console.log(props);
    return (
        <div className="scroll-bottom">
            <div className="sb-main">
                {props.left && (
                    <NavLink to={props.left} className ="left hover">
                        <span>&#10092;</span>prev
                    </NavLink>
                )}
                <p className = "center">Scroll</p>

                {props.right && (
                    <NavLink to={props.right} className ="right hover">
                        <span>&#10093;</span>next
                    </NavLink>
                )}
            </div>
        </div>
    )
}

export default ButtonsButtom
