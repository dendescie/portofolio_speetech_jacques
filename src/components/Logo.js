import React from 'react'

const Logo = () => {
    return (
        <span className="logo">ST</span>
    )
}

export default Logo
