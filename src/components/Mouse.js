import React, { useEffect } from 'react';

const Mouse = () => {
  useEffect(() => {
    const cursor = document.querySelector('.cursor'); // déclencher un evenement

    const handleCursor = (e) => {
      // recuperer le niveau de la souris sur l'axe des y
      cursor.style.top = e.pageY + 'px';

      // recuperer le niveau de la souris sur l'axe des X
      cursor.style.left = e.pageX + 'px';
    };

    window.addEventListener('mousemove', handleCursor);

    const handleHover = () => {
      // Ajoute l'evenement hoverred (reduire la taille de la souris) à la classe cursor
      cursor.classList.add('hovered');
    };

    const handleLeave = () => {
      cursor.style.transition = '.3 s ease-out';
      cursor.classList.remove('hovered');
    };

    document.querySelectorAll('.hover').forEach((link) => {
      link.addEventListener('mouseover', handleHover);
      link.addEventListener('mouseleave', handleLeave);
    });
  }, []);
  return <span className="cursor"></span>;
};

export default Mouse;
