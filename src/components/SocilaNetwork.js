import React from 'react';
// import { Link } from 'react-router-dom';

const SocilaNetwork = () => {
  const anim = () => {
    let navLinks = document.querySelectorAll('.social-network a');

    navLinks.forEach((link) => {
      link.addEventListener('mouseover', (e) => {
        let attribX = e.offsetX - 20;
        let attribY = e.offsetY - 13;

        link.style.transform = `translate(${attribX}px, ${attribY}px)`;
      });
      //ramener les icones à leur place d'origine
      link.addEventListener('mouseleave', () => {
        link.style.transform = ''; //ramène les icones à leur place(Ne fais rien)
      });
    });
  };
  return (
    <div className="social-network">
      <ul className="content">

      <a
          href="https://www.linkedin.com/in/koffi-jacques-agbeti-b535b01bb/details/skills/"
          className="hover"
          rel="nooponer noreferrer"
          target="_blank"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-linkedin"></i>
          </li>
        </a>
        <a
          href="#"
          className="hover"
          rel="nooponer noreferrer"
          target="_blank"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-facebook-f"></i>
          </li>
        </a>
        <a
          href="#"
          className="hover"
          rel="nooponer noreferrer"
          target="_blank"
          onMouseOver={anim}
        >
          <li>
            <i className="fab fa-twitter"></i>
          </li>
        </a>
        
      </ul>
    </div>
  );
};

export default SocilaNetwork;
