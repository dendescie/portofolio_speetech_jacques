export const projectsData = [
  {
    id: 1,
    title: "Skyran Group Store",
    date: "Janvier 2020",
    languages: ["Php", "Wordpress" ,"Woocommerce", "KKIAPAY"],
    infos:
      "Site e-commerce de produits informatiques conçu avec wordpress et l'intégration de woocommerce pour la gestion des ventes et clients, puis le module de payement KKIAPAY pour la gestion des payement elctroniques.",
    img: "./assets/img/projet-1.jpg",
    link: "https://store.skyran-group.com/",
  },
  {
    id: 2,
    title: "Techno Assistance",
    date: "Septembre 2017",
    languages: ["Silvertripe", "Php"],
    infos:
      "Site de téléassistance sur les pannes et conseils informatiques conçu avec le framework/cms Silverstripe avec un panel administrateur dynamique.",
    img: "./assets/img/projet-2.jpg",
    link: "https://techno-assistance.com/",
  },
  {
    id: 3,
    title: "Tout Brille Benin",
    date: "Avril 2021",
    languages: ["Wordpress", "Php",],
    infos:
      "Site d'agence de nettoyage et d'entretien conçu avec le cms Wordpress et une intégration de woocommerce.",
    img: "./assets/img/projet-3.jpg",
    link: "https://toutbrillebenin.com/",
  },
  {
    id: 4,
    title: "Skyran Group",
    date: "Mars 2018",
    languages: ["SilverStripe", "Php"],
    infos:
      "Site vitrine d'une entreprise informatique conçu avec le framework/cms SilverStripe avec un panel administrateur dynamique pour la gestion des produits.",
    img: "./assets/img/projet-4.jpg",
    link: "https://skyran-group.com/",
  },
  {
    id: 5,
    title: "Portofolio",
    date: "Janvier 2022",
    languages: ["React js", "React-router", "Hooks", "Redux"],
    infos:
      "Portofolio conçu avec react js.",
    img: "./assets/img/projet-5n.png",
    link: "#",
  },
];
