import React from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import ButtonsButtom from '../components/ButtonsButtom';
import ContactForm from '../components/ContatctForm';
import Logo from '../components/Logo';
import Mouse from '../components/Mouse';
import Navigation from '../components/Navigation';
import SocilaNetwork from '../components/SocilaNetwork';

const Contact = () => {
  const today= new Date();
  // const monthe= new month();

  return (
    <main>
      <Mouse />
      <div className="contact">
        <Navigation />
        <Logo />
        {/* Contact form  */}
        <ContactForm />

        <div className="contact-infos">
          <div className="address">
            <div className="content">
              <h4> address</h4>
              <p>Cotonou - Bénin</p>
              <p>AKPAKPA GBEDJEWIN </p>
            </div>
          </div>
          <div className="phone">
            <div className="content">
              <h4>Phone</h4>
              <CopyToClipboard text="+22997940000" className="clipboard">
                <p
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    alert('Téléphone copié');
                  }}
                >
                  +229 97 94 17 38
                </p>
              </CopyToClipboard>
            </div>
          </div>

          <div className="email">
            <div className="content">
              <h4>Email</h4>
              <CopyToClipboard text="jacquesagbeti@gmail.com" className="clipboard">
                <p
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    alert('Email copié');
                  }}
                >
                  jacquesagbeti@gmail.com
                </p>
              </CopyToClipboard>
            </div>
          </div>
          <SocilaNetwork />
          <div className="credits">
            <p>fromScracth | &copy; Portofolio SPEETECH - Février {today.getFullYear()}</p>
          </div>
        </div>
        <ButtonsButtom left={'/project-5'} />
      </div>
    </main>
  );
};

export default Contact;
