
import React, { Component } from 'react'
import ButtonsButtom from '../components/ButtonsButtom'
import DynamicText from '../components/DynamicText'
import Mouse from '../components/Mouse'
import Navigation from '../components/Navigation'
import SocilaNetwork from '../components/SocilaNetwork'


const Home = () => {
   
    return (
            
        <main>
        <Mouse  />
            <div className="home">
            <Navigation />
            <SocilaNetwork />
           <div className="home-main">
               <div className="main-content">
                   <h1> SPEETECH</h1>
                   <h2 className="myname"> Jacques AGBETI</h2>
                   <h2>
                       <DynamicText />
                   </h2>
               </div>   
           </div>
           <ButtonsButtom right ={"/project-1"} />
           </div>
        </main>
        
        )
    
}

export default Home

