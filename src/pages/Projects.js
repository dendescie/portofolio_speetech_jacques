import React from 'react';
import ButtonsButtom from '../components/ButtonsButtom';
import DynamicText from '../components/DynamicText';
import Logo from '../components/Logo';
import Mouse from '../components/Mouse';
import Navigation from '../components/Navigation';
import Project from '../components/Project';
import SocilaNetwork from '../components/SocilaNetwork';
import { projectsData } from '../data/projectsData';

export const Project1 = () => {
  return (
    <main>
      <Mouse />
      <div className="project">
        <Project projectNumber={0} />

        <Navigation />
        <Logo />
        <SocilaNetwork />
        <ButtonsButtom left={'/'} right={'/project-2'} />
      </div>
    </main>
  );
};

export const Project2 = () => {
  return (
    <main>
      <Mouse />
      <div className="project">
        <Project projectNumber={1} />

        <Navigation />
        <Logo />
        <SocilaNetwork />
        <ButtonsButtom left={'/project-1'} right={'/project-3'} />
      </div>
    </main>
  );
};

export const Project3 = () => {
  return (
    <main>
      <Mouse />
      <div className="project">
        <Project projectNumber={2} />

        <Navigation />
        <Logo />
        <SocilaNetwork />
        <ButtonsButtom left={'/project-2'} right={'/project-4'} />
      </div>
    </main>
  );
};

export const Project4 = () => {
  return (
    <main>
      <Mouse />
      <div className="project">
        <Project projectNumber={3} />

        <Navigation />
        <Logo />
        <SocilaNetwork />
        <ButtonsButtom left={'/project-3'} right={'/project-5'} />
      </div>
    </main>
  );
};

export const Project5 = () => {
  return (
    <main>
      <Mouse />
      <div className="project">
        <Project projectNumber={4} />

        <Navigation />
        <Logo />
        <SocilaNetwork />
        <ButtonsButtom left={'/project-4'} right={'/contact'} />
      </div>
    </main>
  );
};
